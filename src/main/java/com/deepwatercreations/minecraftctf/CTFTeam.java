package com.deepwatercreations.minecraftctf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.deepwatercreations.minecraftctf.Flag;
import com.deepwatercreations.minecraftctf.FlagStateChangeEvent;
import com.deepwatercreations.minecraftctf.zones.Zone;

public class CTFTeam implements Listener{

	public static final ChatColor[] validTeamColors = {
		ChatColor.RED, ChatColor.BLUE, 
		ChatColor.WHITE, ChatColor.BLACK, 
		ChatColor.YELLOW, ChatColor.DARK_GRAY,
	       	ChatColor.AQUA, ChatColor.DARK_PURPLE,
	       	ChatColor.GOLD, ChatColor.GRAY, 
		ChatColor.DARK_AQUA, ChatColor.GREEN, 
		ChatColor.LIGHT_PURPLE, ChatColor.DARK_GREEN 	
	};

	public static Map<String, CTFTeam> teamDict = new HashMap<String, CTFTeam>();

	public static CTFTeam getTeamOfPlayer(Player player){
		for(CTFTeam team : teamDict.values()){
			if(team.hasPlayer(player)){
				return team;
			}
		}
		return null;
	}

	public static CTFTeam getEnemyTeam(CTFTeam team){
		for(CTFTeam otherteam : teamDict.values()){
			if(!team.equals(otherteam)){
				return otherteam;
			}
		}
		return null;
	}

	public String name;
	public String scoreName;
	public ChatColor color;

	public Scoreboard scoreboard;
	public Team scoreboardTeam;

	public Location teamBaseLoc;
	public Zone zone;
	public Flag flag;

	private List<Player> players;

	public CTFTeam(String name, ChatColor color, MinecraftCTF plugin, Scoreboard scoreboard, Objective scoreObjective, Location teamBaseLoc, int teamZoneRadius){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);

		this.name = name;
		this.color = color;
		this.scoreName = color + name;

		this.scoreboard = scoreboard;
		this.scoreboardTeam = scoreboard.registerNewTeam(name);
		scoreboardTeam.setColor(color);
		scoreObjective.getScore(this.scoreName).setScore(0);

		this.teamBaseLoc = teamBaseLoc;
		this.zone = new Zone(plugin, teamBaseLoc, teamZoneRadius, Zone.getParticleColorFromChatColor(color));
		this.flag = new Flag(plugin, teamBaseLoc, color, this);

		this.players = new ArrayList<Player>();

		CTFTeam.teamDict.put(name, this);
	}

	public boolean hasPlayer(Player player){
		return this.players.contains(player);
	}

	public void addPlayer(Player player){
		this.scoreboardTeam.addEntry(player.getName());
		this.players.add(player);
	}

	//Update player compasses when enemy flag changes state
	@EventHandler(priority = EventPriority.MONITOR)
	public void onFlagStateChange(FlagStateChangeEvent event){
		Flag changedFlag = event.getFlag();
		if(!changedFlag.equals(this.flag)){
			for(Player player : this.players){
				player.setCompassTarget(changedFlag.getLocation());
			}
		}
	}
}
