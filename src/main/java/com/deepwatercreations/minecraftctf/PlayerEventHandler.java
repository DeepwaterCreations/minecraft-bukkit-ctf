package com.deepwatercreations.minecraftctf;

import de.tr7zw.changeme.nbtapi.NBTItem;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.CompassMeta;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class PlayerEventHandler implements Listener{

	private MinecraftCTF plugin;
	private Objective scoreObjective;
	private Scoreboard scoreboard;

	public PlayerEventHandler(MinecraftCTF plugin, Objective scoreObjective, Scoreboard scoreboard){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);

		this.plugin = plugin;
		this.scoreObjective = scoreObjective;
		this.scoreboard = scoreboard;
	}


	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event){
		Player player = event.getPlayer();
		Location pLoc = player.getLocation();

		//Check if they've standing on their own flag
		CTFTeam ownTeam = CTFTeam.getTeamOfPlayer(player);
		if(ownTeam != null){
			Flag ownFlag = Flag.getFlagForTeamName(ownTeam.name);
			if(ownFlag != null && ownFlag.getFlagState() == Flag.FlagState.BLOCK){
				Location fLoc = ownFlag.getLocation();
				if(pLoc.getBlockX() == fLoc.getBlockX() &&
				   pLoc.getBlockY() == fLoc.getBlockY() &&
				   pLoc.getBlockZ() == fLoc.getBlockZ()){
					if(!ownTeam.zone.isInBounds(pLoc)) {
						player.sendMessage("Can't score outside your team zone"); 
					} else {
						//They're standing on their own flag's block and in their own zone, so check if they can score.
						Score ownTeamScore = this.scoreObjective.getScore(ownTeam.scoreName);
						List<ItemStack> carriedFlags = Flag.getFlagItemsFromInventory(player.getInventory());
						//Score for each flag in their inventory. We assume if they're standing on their own
						//team's flag block, that flag probably isn't also in their inventory, so every flag they're carrying
						//is a flag they can score off of.
						for(ItemStack carriedFlag : carriedFlags){
							//Increase the player's team's score
							ownTeamScore.setScore(ownTeamScore.getScore() + 1);
							//Figure out whose flag it is
							NBTItem nbtitem = new NBTItem(carriedFlag);
							String flagTeamName = nbtitem.getString(MinecraftCTF.TEAM_KEY);
							Flag cappedFlag = Flag.getFlagForTeamName(flagTeamName);
							CTFTeam cappedFlagTeam = cappedFlag.team;
							//Respawn it
							cappedFlag.respawn();
							//and spread the news!
							plugin.getServer().broadcastMessage(String.format("%s captured team %s's flag!", player.getName(), CTFCommandExecutor.teamColoredText(cappedFlagTeam, cappedFlagTeam.name)));
							plugin.getServer().broadcastMessage(CTFCommandExecutor.teamColoredText(ownTeam, String.format("TEAM %s SCORES!", ownTeam.name)));
							//TODO: Sound effects, particle effects?
						}
						//TODO: Check for win (maybe even emit an event? Only if I have a genuine use for it - YNGNI)
					}
				}
			} 
		} 
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event){
		Player player = event.getEntity();
		//Spawn them at their base if they don't have a different spawn set
		if(player.getBedSpawnLocation() == null){
			CTFTeam team = CTFTeam.getTeamOfPlayer(player);
			if(team == null){
				return;
			}
			Flag flag = Flag.getFlagForTeamName(team.name);
			Block spawnBlock = flag.spawnBlock;
			Location spawnLoc;
			if(spawnBlock != null){
				spawnLoc = spawnBlock.getLocation().clone().add(0,1,0);
			} else {
				spawnLoc = flag.initLoc.clone().add(0,1,0);
			}
			player.setBedSpawnLocation(spawnLoc, true);
			//Clear any clutter blocks out of the way so the player can actually spawn
			for(int y = 0; y < 2; y++){
				if(!spawnLoc.getBlock().isPassable()){
					spawnLoc.getBlock().setType(Material.AIR);
				}
				spawnLoc.add(0,1,0);
			}
		}
	}

	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event){
		Player player = event.getPlayer();
		givePlayerCompass(player);
	}


	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event){
		Player player = event.getPlayer();		
		Flag.forcePlayerDropFlags(player);
	}



	public static void givePlayerCompass(Player player){
		ItemStack compass = new ItemStack(Material.COMPASS);
		CompassMeta meta = (CompassMeta) compass.getItemMeta();
		meta.setDisplayName("Enemy Flag");
		meta.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
		compass.setItemMeta(meta);
		player.getInventory().addItem(compass); 
	}

}
