package com.deepwatercreations.minecraftctf;

import de.tr7zw.changeme.nbtapi.NBTItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.type.EndPortalFrame;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import com.deepwatercreations.minecraftctf.MinecraftCTF;

public class Flag implements Listener{

	public static Map<String, Flag> flagsByTeamname = new HashMap<String, Flag>();

	public static String FLAG_KEY = "CTF_FLAG"; //for setting the flag flag
							//TODO: It occurs to me that I can make the 
							//	existing key "TEAM_FLAG_KEY" and just
							//	assume anything that has it is a flag.

	FlagState flagState;
	Block block; //Null if the banner isn't placed as a block
	Block attachedBlock;
	Block spawnBlock;
	Material bannerType;
	ItemStack item; //Never changes
	CTFTeam team;
	Location initLoc;
	Inventory container;
	Item itemEntity;

	public Flag(MinecraftCTF plugin, Location initLoc, ChatColor color, CTFTeam team){

		plugin.getServer().getPluginManager().registerEvents(this, plugin);

		this.spawnBlock = initLoc.clone().getBlock();
		this.spawnBlock.setType(Material.END_PORTAL_FRAME);
		EndPortalFrame blockData = (EndPortalFrame) Material.END_PORTAL_FRAME.createBlockData();
		blockData.setEye(true);
		this.spawnBlock.setBlockData(blockData);

		this.block = initLoc.clone().add(0,1,0).getBlock();
		this.bannerType = Flag.getBannerForColor(color);
		this.block.setType(bannerType);

		this.attachedBlock = spawnBlock;

		this.flagState = FlagState.BLOCK;

		this.bannerType = bannerType;
		this.team = team;
		this.initLoc = initLoc;

		//Make the flag item
		ItemStack flagItem = new ItemStack(this.bannerType);
		ItemMeta meta = flagItem.getItemMeta();
		meta.setDisplayName(this.team.name);
		flagItem.setItemMeta(meta);
		//We need to use an external library to set NBT data
		//or else two flags with the same name will count as the same flag.
		NBTItem nbtitem = new NBTItem(flagItem);
		nbtitem.setString(MinecraftCTF.TEAM_KEY, team.name); //TODO: Consider using a more specific key name
		nbtitem.setBoolean(Flag.FLAG_KEY, true); //This just means "this is a flag".
		flagItem = nbtitem.getItem();
		this.item = flagItem;
		this.container = null;
		this.itemEntity = null;

		Flag.flagsByTeamname.put(team.name, this);
	}

	public Location getLocation(){
		//TODO: onFlagStateChange event so that compasses can update.
		//TODO: some kind of ChangeState method so that I can update holder/itementity/so forth
		//	more properly	
		switch(this.flagState){
			case BLOCK:
				return this.block.getLocation();
			case ITEM:
				return this.container.getLocation();
			case ITEMENTITY:
				return this.itemEntity.getLocation();
		}
		return null;
	}

	public FlagState getFlagState(){
		return this.flagState;
	}

	private void setFlagState(FlagState newState){
		FlagState oldState = this.getFlagState();
		this.flagState = newState;
		if(newState != FlagState.BLOCK){
			this.block = null;
			this.attachedBlock = null;
		} 
		if(newState != FlagState.ITEM){
			this.container = null;
			//this.item does not become null. (Should I change this?)
		} else if(newState != FlagState.ITEMENTITY){
			this.itemEntity = null;
		}
		FlagStateChangeEvent event  = new FlagStateChangeEvent(this, oldState, newState);
		Bukkit.getServer().getPluginManager().callEvent(event);
	}

	//Moves *this* flag back to spawn from wherever it is in the world.
	public void respawn(){
		World world = this.initLoc.getWorld();
		if(this.block != null){
			this.block.setType(Material.AIR);
			//TODO: Check if spawn block still exists and if not, respawn it too
		}
		for(Player player : world.getPlayers()){
			Inventory inv = player.getInventory();
			for(ItemStack itemStack : inv){
				if(isThisFlag(itemStack)){
					itemStack.setAmount(0);
				}
			}
		}
		//TODO: use world.getNearbyEntities() with the game zone bounding box and a Predicate
		for(Item itemEntity : world.getEntitiesByClass(Item.class)){
			ItemStack itemStack = itemEntity.getItemStack();
			if(isThisFlag(itemStack)){
				itemStack.setAmount(0);
			}
		}
		this.block = this.spawnBlock.getRelative(0,1,0);
		this.block.setType(this.bannerType);
		this.attachedBlock = this.spawnBlock;
		this.setFlagState(FlagState.BLOCK);
		this.container = null;
		this.itemEntity = null;
		//TODO: Refactor this biz.
		//TODO: Respawn flags that are in chests and the like
		//TODO: Separate despawn and spawn methods? I might want to spawn flags without
		//	checking for despawn if, frex, one falls into lava.
		//TODO: Check to make sure the enemy flag isn't on the spawn block when respawning
		//	(It'll replace the block, but when the flag is broken, both flags will drop.)
	}

	public boolean isThisFlag(ItemStack someItem){
		if(someItem != null && !(someItem.getType().equals(Material.AIR))){
			NBTItem nbtitem = new NBTItem(someItem);
			if(nbtitem.hasKey(MinecraftCTF.TEAM_KEY) && nbtitem.getString(MinecraftCTF.TEAM_KEY).equals(this.team.name)){
				return true;
			}
		}
		return false;
	}

	/* BLOCK EVENTS */

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		if(this.getFlagState() == FlagState.BLOCK &&
		   (event.getBlock().equals(this.block) || 
		    event.getBlock().equals(this.attachedBlock))){
			event.getPlayer().sendMessage("You broke a flag");

			//Stop this event from dropping a natural flag
			event.setDropItems(false);

			//Drop the custom flag right onto the player's location to make it
			//a little harder to build stuff that makes it impossible to pick
			//up the flag: //TODO: don't do this outside of team zones?
			Player player = event.getPlayer();
			World world = player.getWorld();
			this.itemEntity = world.dropItem(player.getLocation(), this.item);
			this.setFlagState(FlagState.ITEMENTITY);
		    }
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event){
		if(this.getFlagState() == FlagState.ITEM &&
		   this.item != null && 
		   !event.isCancelled() && 
		   event.getItemInHand().equals(this.item)){
			event.getPlayer().sendMessage("You placed a flag");

			//Set the new block location
			this.block = event.getBlockPlaced();
			this.setFlagState(FlagState.BLOCK);

			if(this.block.getBlockData() instanceof Directional){
				//It's a wall banner, so the attached block is dependent on 
				//which way it's facing.
				BlockFace facing = ((Directional) this.block.getBlockData()).getFacing();
				this.attachedBlock = this.block.getRelative(facing.getOppositeFace());

			} else {
				//It's a regular banner, so the attached block is right under it.
				this.attachedBlock = this.block.getRelative(0, -1, 0);
			}
		}
	}

	/* ITEM ENTITY EVENTS */

	// @EventHandler
	// public void onPlayerDropItem(PlayerDropItemEvent event){
		////This doesn't apparently fire when the flag is "dropped" because
		////the player went out of bounds.
		////Probably that's fixable by deliberately spawning an event in Zone
		////or something.
		//Item spawnItem = event.getItemDrop();
		//// Player dropper = event.getPlayer();
		//ItemStack spawnItemStack = spawnItem.getItemStack();
		//if(this.item != null &&
		//   spawnItemStack.isSimilar(this.item) &&
		//   Flag.sWhatThisItemStackIs(spawnItemStack)){
		//	this.flagState = FlagState.ITEMENTITY;
		//	Bukkit.getServer().broadcastMessage("Flag dropped");
		//}
	// }
	
	@EventHandler
	public void onItemSpawn(ItemSpawnEvent event){
		Item spawnItem = event.getEntity();
		ItemStack spawnItemStack = spawnItem.getItemStack();
		if(this.getFlagState() == FlagState.ITEM &&
		   this.item != null &&
		   spawnItemStack.isSimilar(this.item) &&
		   Flag.sWhatThisItemStackIs(spawnItemStack)){
			this.itemEntity = spawnItem;
			this.setFlagState(FlagState.ITEMENTITY);
			Bukkit.getServer().broadcastMessage("Flag item spawned");
		}
	}

	//Stops flag item entities from despawning after 5 minutes
	@EventHandler
	public void onItemDespawn(ItemDespawnEvent event){
		Item spawnItem = event.getEntity();
		ItemStack spawnItemStack = spawnItem.getItemStack();
		if(this.getFlagState() == FlagState.ITEMENTITY &&
		   this.item != null &&
		   spawnItemStack.isSimilar(this.item) && 
		   Flag.sWhatThisItemStackIs(spawnItemStack)){
			event.setCancelled(true); //Item exists for 5 more minutes
		}
	}

	//Respawns flag when it's destroyed
	@EventHandler
	public void onItemDamage(EntityDamageEvent event){
		if(this.getFlagState() == FlagState.ITEMENTITY &&
		   event.getEntityType().equals(EntityType.DROPPED_ITEM)){
			Item damagedItem = (Item) event.getEntity();
			ItemStack damagedItemStack = damagedItem.getItemStack();
			if(this.item != null &&
			   damagedItemStack.isSimilar(this.item) && 
			   Flag.sWhatThisItemStackIs(damagedItemStack) &&
			   event.getFinalDamage() > 0 &&
			   !event.isCancelled()){
				damagedItem.remove();
				this.respawn(); //TODO: Maybe don't respawn *both* the flags?
			}
		}
	}

	@EventHandler
	public void onEntityPickupItem(EntityPickupItemEvent event){
		Item item = event.getItem();
		ItemStack itemStack = item.getItemStack();
		if(this.getFlagState() == FlagState.ITEMENTITY &&
		   this.item != null &&
		   itemStack.isSimilar(this.item) &&
		   Flag.sWhatThisItemStackIs(itemStack)){
			if(event.getEntityType() != EntityType.PLAYER){
				event.setCancelled(true);
				//TODO: Maybe I can let non-players pick up flags?
			} else {
				Bukkit.getServer().broadcastMessage("Flag picked up");
				this.container = ((Player) event.getEntity()).getInventory();
				this.setFlagState(FlagState.ITEM);
			}
		   }
	}

	@EventHandler
	public void onInventoryPickupItem(InventoryPickupItemEvent event){
		Item item = event.getItem();
		ItemStack itemStack = item.getItemStack();
		if(this.getFlagState() == FlagState.ITEMENTITY &&
		   this.item != null &&
		   itemStack.isSimilar(this.item) &&
		   Flag.sWhatThisItemStackIs(itemStack)){
			   Bukkit.getServer().broadcastMessage("Flag picked up");
			   this.container = event.getInventory();
			   this.setFlagState(FlagState.ITEM);
		   }
	}

	/*	ITEM EVENTS	*/

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event){
		if(this.getFlagState() == FlagState.ITEM &&
		   event.getInventory().equals(this.container)){
			HumanEntity human = event.getPlayer();
			Inventory playerInv = human.getInventory();
			Inventory closedInv = event.getInventory();
			Inventory changedInv = null;
			if(!Flag.getFlagItemsFromInventory(playerInv).isEmpty()){
				changedInv = playerInv;
			} else if(!Flag.getFlagItemsFromInventory(closedInv).isEmpty()){
				changedInv = closedInv;
			} else {
				Bukkit.getLogger().warning("Flag should be in container or player but isn't");
			}
			
			//TODO: This doesn't work
			if(changedInv != null && !changedInv.equals(this.container)){
				this.container = changedInv;
				this.setFlagState(FlagState.ITEM);
			}
		   }
	}


	/*	STATIC METHODS     */

	public static boolean sWhatThisItemStackIs(ItemStack stack){
		if(stack != null){
			NBTItem nbtitem = new NBTItem(stack);
			return nbtitem.hasKey(Flag.FLAG_KEY);
		} else {
			return false;
		}
	}

	public static List<ItemStack> getFlagItemsFromInventory(Inventory inv){
		List<ItemStack> flags = new ArrayList<ItemStack>();
		for(ItemStack item : inv){
			if(item != null){
				NBTItem nbtitem = new NBTItem(item);
				if(nbtitem.hasKey(Flag.FLAG_KEY)){
					flags.add(item);
				}		
			}
		}
		return flags;
	}

	public static Flag getFlagForTeamName(String teamName){
		return Flag.flagsByTeamname.get(teamName); //Returns null if no value for key
	}

	//TODO: Make sure this actually works.
	//	My suspicion is that removeFlagsFromInv will delete the item
	//	stacks on the ground, not just the ones in the player's inventory.
	//	Also need to be sure the NBT stuff carries over.
	public static void forcePlayerDropFlags(Player player){
		List<ItemStack> flagItems = Flag.getFlagItemsFromInventory(player.getInventory());
		//Create the flags on the ground
		World world = player.getWorld();
		Location playerLoc = player.getLocation();
		for(ItemStack flagItem : flagItems){
			world.dropItemNaturally(playerLoc, flagItem);
		}
		//Remove the flags from their inventory
		Flag.removeFlagsFromInv(player);
	}

	//TODO: This should set the flags' holders to null, right?
	public static void removeFlagsFromInv(Player player){
		Inventory inv = player.getInventory();
		for(ItemStack item : inv){
			if(item != null && !(item.getType().equals(Material.AIR))){
				NBTItem nbtitem = new NBTItem(item);
				if(nbtitem.hasKey(MinecraftCTF.TEAM_KEY)){
					item.setAmount(0);
				}
			}
		}
	}

	public static void resetList(){
		for(Flag flag : Flag.flagsByTeamname.values()){
			flag.respawn();
			if(flag.item != null){
				flag.item = null;
			}
			if(flag.block != null){
				flag.block.setType(Material.AIR);
				flag.block = null;
			}
			if(flag.spawnBlock != null){
				flag.spawnBlock.setType(Material.AIR);
				flag.spawnBlock = null;
			}
		}
		Flag.flagsByTeamname = new HashMap<String, Flag>();
	}

	public static Material getBannerForColor(ChatColor color) throws IllegalArgumentException{
		switch(color){
			case AQUA:
				return Material.LIGHT_BLUE_BANNER;
			case BLACK:
				return Material.BLACK_BANNER;
			case BLUE:
				return Material.BLUE_BANNER;
			case DARK_AQUA:
				return Material.CYAN_BANNER;
			// case DARK_BLUE:
			// 	return Material._BANNER;
			case DARK_GRAY:
				return Material.GRAY_BANNER;
			case DARK_GREEN:
				return Material.GREEN_BANNER;
			case DARK_PURPLE:
				return Material.PURPLE_BANNER;
			// case DARK_RED:
			// 	return Material.%_BANNER;
			case GOLD:
				return Material.ORANGE_BANNER;
			case GRAY:
				return Material.LIGHT_GRAY_BANNER;
			case GREEN:
				return Material.LIME_BANNER;
			case LIGHT_PURPLE:
				return Material.MAGENTA_BANNER;
			case RED:
				return Material.RED_BANNER;
			case WHITE:
				return Material.WHITE_BANNER;
			case YELLOW:
				return Material.YELLOW_BANNER;
			default:
				throw new IllegalArgumentException(String.format("No matching banner for color %s", color.toString()));
		}
	}

	//TODO:
	//Break portal frame block? How do I inform the players this is possible?
	//Check if flag is destroyed from: Despawn, BlockBurnedEvent(?), maybe just
	//	periodically.
	//Different placement and pickup behavior by team? Or, zone-aware?
	
	public enum FlagState{
		BLOCK, ITEM, ITEMENTITY
	}
}
