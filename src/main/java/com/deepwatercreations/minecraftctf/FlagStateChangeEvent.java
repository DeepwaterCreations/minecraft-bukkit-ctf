package com.deepwatercreations.minecraftctf;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import com.deepwatercreations.minecraftctf.Flag;

public final class FlagStateChangeEvent extends Event{
	private static final HandlerList handlers = new HandlerList();

	private Flag flag;
	private Flag.FlagState oldState;
	private Flag.FlagState newState;
	public FlagStateChangeEvent(Flag flag, Flag.FlagState oldState, Flag.FlagState newState){
		this.flag = flag;
		this.oldState = oldState;	
		this.newState = newState;	
	}

	public Flag getFlag(){
		return this.flag;
	}

	public Flag.FlagState getOldState(){
		return this.oldState;
	}

	public Flag.FlagState getNewState(){
		return this.newState;
	}

	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList(){
		return handlers;
	}
}
